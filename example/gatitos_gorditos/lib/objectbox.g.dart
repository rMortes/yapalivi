// GENERATED CODE - DO NOT MODIFY BY HAND

// ignore_for_file: camel_case_types

import 'dart:typed_data';

import 'package:objectbox/flatbuffers/flat_buffers.dart' as fb;
import 'package:objectbox/internal.dart'; // generated code can access "internal" functionality
import 'package:objectbox/objectbox.dart';

import 'models/Cat.dart';

export 'package:objectbox/objectbox.dart'; // so that callers only have to import this file

final _entities = <ModelEntity>[
  ModelEntity(
      id: const IdUid(1, 74766340126393633),
      name: 'Cat',
      lastPropertyId: const IdUid(4, 5919188168133699667),
      flags: 0,
      properties: <ModelProperty>[
        ModelProperty(
            id: const IdUid(1, 2749564907851188501),
            name: 'id',
            type: 6,
            flags: 1),
        ModelProperty(
            id: const IdUid(2, 9163128333858282098),
            name: 'uuid',
            type: 9,
            flags: 2048,
            indexId: const IdUid(1, 6688337085863921047)),
        ModelProperty(
            id: const IdUid(3, 283625857412429455),
            name: 'created_at',
            type: 10,
            flags: 0),
        ModelProperty(
            id: const IdUid(4, 5919188168133699667),
            name: 'tags',
            type: 30,
            flags: 0)
      ],
      relations: <ModelRelation>[],
      backlinks: <ModelBacklink>[])
];

/// ObjectBox model definition, pass it to [Store] - Store(getObjectBoxModel())
ModelDefinition getObjectBoxModel() {
  final model = ModelInfo(
      entities: _entities,
      lastEntityId: const IdUid(1, 74766340126393633),
      lastIndexId: const IdUid(1, 6688337085863921047),
      lastRelationId: const IdUid(0, 0),
      lastSequenceId: const IdUid(0, 0),
      retiredEntityUids: const [],
      retiredIndexUids: const [],
      retiredPropertyUids: const [],
      retiredRelationUids: const [],
      modelVersion: 5,
      modelVersionParserMinimum: 5,
      version: 1);

  final bindings = <Type, EntityDefinition>{
    Cat: EntityDefinition<Cat>(
        model: _entities[0],
        toOneRelations: (Cat object) => [],
        toManyRelations: (Cat object) => {},
        getId: (Cat object) => object.id,
        setId: (Cat object, int id) {
          object.id = id;
        },
        objectToFB: (Cat object, fb.Builder fbb) {
          final uuidOffset = fbb.writeString(object.uuid);
          final tagsOffset = fbb.writeList(
              object.tags.map(fbb.writeString).toList(growable: false));
          fbb.startTable(5);
          fbb.addInt64(0, object.id ?? 0);
          fbb.addOffset(1, uuidOffset);
          fbb.addInt64(2, object.created_at.millisecondsSinceEpoch);
          fbb.addOffset(3, tagsOffset);
          fbb.finish(fbb.endTable());
          return object.id ?? 0;
        },
        objectFromFB: (Store store, ByteData fbData) {
          final buffer = fb.BufferContext(fbData);
          final rootOffset = buffer.derefObject(0);

          final object = Cat(
              id: const fb.Int64Reader()
                  .vTableGetNullable(buffer, rootOffset, 4),
              uuid:
                  const fb.StringReader().vTableGet(buffer, rootOffset, 6, ''),
              created_at: DateTime.fromMillisecondsSinceEpoch(
                  const fb.Int64Reader().vTableGet(buffer, rootOffset, 8, 0)),
              tags: const EagerListReader<String>(fb.StringReader())
                  .vTableGet(buffer, rootOffset, 10, []));

          return object;
        })
  };

  return ModelDefinition(model, bindings);
}

/// [Cat] entity fields to define ObjectBox queries.
class Cat_ {
  /// see [Cat.id]
  static final id = QueryIntegerProperty<Cat>(_entities[0].properties[0]);

  /// see [Cat.uuid]
  static final uuid = QueryStringProperty<Cat>(_entities[0].properties[1]);

  /// see [Cat.created_at]
  static final created_at =
      QueryIntegerProperty<Cat>(_entities[0].properties[2]);

  /// see [Cat.tags]
  static final tags =
      QueryStringVectorProperty<Cat>(_entities[0].properties[3]);
}
