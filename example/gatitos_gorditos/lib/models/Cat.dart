import 'dart:convert';

import 'package:objectbox/objectbox.dart';

@Entity()
class Cat {
  int? id;

  @Index()
  String uuid;

  DateTime created_at;

  List<String> tags;

  Cat(
      {this.id,
      required this.uuid,
      required this.created_at,
      required this.tags});

  Cat.fromMap(Map<String, dynamic> map)
      : this.uuid = map['id'],
        this.created_at = DateTime.parse(map['created_at']),
        this.tags = List<String>.from(map['tags']);

  static Cat fromJson(String json) => Cat.fromMap(jsonDecode(json));
}
