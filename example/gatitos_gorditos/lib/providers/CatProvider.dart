import 'dart:convert';

import 'package:connectivity/connectivity.dart';
import 'package:gatitos_gorditos/models/Cat.dart';
import 'package:gatitos_gorditos/objectbox.g.dart';
import 'package:get_it/get_it.dart';
import 'package:http/http.dart' as http;
import 'package:objectbox/objectbox.dart';
import 'package:yapalivi/yapalivi.dart';

const PAGE_SIZE = 20;

class CatProvider extends IAutomaticLocalPagedProvider<Cat> {
  Future<List<Cat>> fetchRemotePage(int page) async {
    final limit = PAGE_SIZE;
    final skip = page * PAGE_SIZE;

    final res = await http.get(Uri.https("cataas.com", "/api/cats",
        {"limit": limit.toString(), "skip": skip.toString()}));

    final List<Map<String, dynamic>> body =
        List<Map<String, dynamic>>.from(jsonDecode(utf8.decode(res.bodyBytes)));
    final List<Cat> cats =
        List.generate(body.length, (index) => Cat.fromMap(body[index]));

    (() async {
      final store = await GetIt.I.getAsync<Store>();
      final catBox = store.box<Cat>();

      for (final cat in cats) {
        final query = catBox.query(Cat_.uuid.equals(cat.uuid)).build();
        final storedCat = query.find();
        if (storedCat.isNotEmpty) cat.id = storedCat.first.id;
      }

      catBox.putMany(cats);
    })();

    return cats;
  }

  @override
  Future<List<Cat>> fetchLocalPage(int page) async {
    final store = await GetIt.I.getAsync<Store>();
    final catBox = store.box<Cat>();

    final query = catBox.query().build()
      ..limit = PAGE_SIZE
      ..offset = page * PAGE_SIZE;

    final cats = query.find();
    return cats;
  }

  @override
  Future<bool> shouldFetchLocal() => Connectivity()
      .checkConnectivity()
      .then((result) => result == ConnectivityResult.none);

  @override
  Future<void> tryLoadingNewPage(setState) async {
    final newCats = await fetchPage(currentPage++);
    if (newCats.length < PAGE_SIZE) lastPageFetched = true;
    setState(() {
      content.addAll(newCats);
    });
  }
}
