import 'package:flutter/material.dart';
import 'package:gatitos_gorditos/models/Cat.dart';
import 'package:gatitos_gorditos/objectbox.g.dart';
import 'package:gatitos_gorditos/providers/CatProvider.dart';
import 'package:get_it/get_it.dart';
import 'package:objectbox/objectbox.dart';
import 'package:path_provider/path_provider.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:yapalivi/widget/Yapalivi.dart';

void main() {
  GetIt.I.registerSingletonAsync<Store>(() => getApplicationDocumentsDirectory()
      .then((dir) =>
          Store(getObjectBoxModel(), directory: dir.path + '/objectbox')));
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.purple,
      ),
      home: MyHomePage(title: 'Gatitos gorditos'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  /// Esta es la página principal.
  /// Contiene una lista de fotos de gatitos bonitos
  MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  /// Este es el "proveedor" de los gatitos. Se encarga de dar la información
  /// en bruto a la lista paginada
  late final CatProvider _pagedProvider;

  /// Este es el controlador del campo de texto que hay en el pseudo FAB
  final TextEditingController _textEditingController = TextEditingController();

  /// Este es el string que se utiliza para filtrar los gatitos
  String? filter;

  /// Este booleano marca si el FAB debería de estar colapsado o completo
  bool fabCollapsed = false;

  @override
  void initState() {
    super.initState();

    /// El provider se establece cuando se crea el estado.
    /// De esta forma, el estado es persistente entre setState's
    _pagedProvider = CatProvider();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        /// El título de la app tiene un enlace a la cuenta de twitter de
        /// GatitosGorditos (recomendada) porque me inspiró para el nombre
        /// y quería saber si era posible
        title: GestureDetector(
            onTap: () => launch("https://twitter.com/GorditoGatito2"),
            child: Text(widget.title)),
      ),

      /// El cuerpo es un Stack para poder poner el pseudo FAB sin problemas
      body: Stack(
        children: [
          /// La magia de la situación: Nuestra propia lista paginada
          Yapalivi<Cat>(
            /// Se le hay que pasar el proveedor
            provider: _pagedProvider,

            /// Opcionalmente se le puede pasar una función de filtro (quizás
            /// se podría establecer en el proveedor) para limitar que contenido
            /// se "construye" en la lista o no
            filterFunction: filter != null
                ? (elements) => elements.where((element) =>
                    element.tags.any((element) => element.contains(filter!)))
                : null,

            /// Este es el widget "placeholder", que se ve al final de la lista
            /// siempre que no se haya llegado al final. Siempre se muestra,
            /// pero como cuando lo ves ya estás cargando la siguiendo página,
            /// da igual
            placeholderWidget: Padding(
              padding: const EdgeInsets.only(top: 24),
              child: Center(child: CircularProgressIndicator()),
            ),

            /// Este es el widget que se ve al final de la lista cuando no
            /// hay más datos que mostrar. De determinar si hay o no más datos
            /// se encarga el provider
            noMoreItemsWidget: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 24),
              child: Center(child: Text("No more cute cats to load 😺")),
            ),

            /// El builder se encarga de pasar el índice, por si fuera necesario,
            /// y el objecto para facilidad del consumidor
            itemBuilder: (context, index, item) {
              /// Estilo miscelaneo
              final img = Column(children: [
                Container(
                  clipBehavior: Clip.antiAlias,
                  margin: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: Colors.white,
                      boxShadow: [
                        BoxShadow(
                            blurRadius: 4,
                            offset: Offset(0, 4),
                            color: Color.fromRGBO(0, 0, 0, .25))
                      ]),
                  child: Row(
                    children: [
                      Expanded(
                        child: GestureDetector(
                          onTap: () {
                            Navigator.of(context).push(PageRouteBuilder(
                              opaque: false,
                              pageBuilder: (_, __, ___) => ImageRoute(
                                item: item,
                              ),
                            ));
                          },
                          child: imageBuilder(item),
                        ),
                      )
                    ],
                  ),
                ),
                SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  physics: BouncingScrollPhysics(),
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        for (final tag in item.tags)
                          Container(
                            clipBehavior: Clip.antiAlias,
                            margin: EdgeInsets.all(5),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                color: Colors.white,
                                boxShadow: [
                                  BoxShadow(
                                      blurRadius: 4,
                                      offset: Offset(0, 4),
                                      color: Color.fromRGBO(0, 0, 0, .25))
                                ]),
                            child: Material(
                                child: InkWell(
                              onTap: () {
                                setState(() {
                                  _textEditingController.text = tag;
                                  filter = tag;
                                  fabCollapsed = true;
                                });
                              },
                              child: Padding(
                                padding: EdgeInsets.all(5),
                                child: Text(tag),
                              ),
                            )),
                          )
                      ]),
                )
              ]);

              /// No encontré mejor manera de hacer esto, pero funciona
              return index % PAGE_SIZE == 0
                  ? Column(
                      children: [
                        Container(
                            padding: EdgeInsets.all(10),
                            margin: EdgeInsets.symmetric(vertical: 5),
                            decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(10),
                                boxShadow: [
                                  BoxShadow(
                                      blurRadius: 4,
                                      offset: Offset(0, 4),
                                      color: Color.fromRGBO(0, 0, 0, .25))
                                ]),
                            child: Text("Pagina ${index ~/ PAGE_SIZE}")),
                        img
                      ],
                    )
                  : img;
            },
          ),

          /// Hic sunt pseudo FAB
          Align(
              alignment: Alignment.bottomRight,
              child: floatingActionButtonForm()),
        ],
      ),
    );
  }

  Widget floatingActionButtonForm() {
    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: Container(
        constraints: BoxConstraints(minHeight: 50),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            color: Colors.white,
            boxShadow: [
              BoxShadow(
                  color: Color.fromRGBO(0, 0, 0, .25),
                  blurRadius: 4,
                  offset: Offset(0, 4))
            ]),
        child: GestureDetector(
          onTap: () {
            setState(() {
              fabCollapsed = !fabCollapsed;
            });
          },
          child: AnimatedCrossFade(
            duration: Duration(milliseconds: 200),
            crossFadeState: fabCollapsed
                ? CrossFadeState.showSecond
                : CrossFadeState.showFirst,
            firstChild: Container(
              constraints: BoxConstraints(minHeight: 50),
              child: Row(
                children: [
                  Expanded(
                      child: Container(
                          padding: EdgeInsets.symmetric(horizontal: 10),
                          child: TextFormField(
                            controller: _textEditingController,
                            style: TextStyle(fontSize: 16),
                            textInputAction: TextInputAction.search,
                            decoration:
                                InputDecoration(labelText: "Busca tags"),
                            onEditingComplete: () {
                              setState(() {
                                filter = _textEditingController.text;
                                fabCollapsed = true;
                              });
                              final currentFocus = FocusScope.of(context);
                              if (!currentFocus.hasPrimaryFocus)
                                currentFocus.unfocus();
                            },
                          ))),
                  Container(
                      width: 50,
                      height: 50,
                      child: Icon(Icons.arrow_forward_ios))
                ],
              ),
            ),
            secondChild:
                Container(width: 50, height: 50, child: Icon(Icons.search)),
          ),
        ),
      ),
    );
  }
}

/// Como utilizo el builder este en dos páginas lo he sacado fuera
Widget imageBuilder(Cat item) {
  return Hero(
    tag: item.uuid,
    child: Image.network("https://cataas.com/cat/${item.uuid}",
        fit: BoxFit.fitWidth,
        loadingBuilder: (context, child, loadingProgress) {
      final progress = (loadingProgress?.cumulativeBytesLoaded ?? 0) /
          (loadingProgress?.expectedTotalBytes ?? -1);
      if (progress < 1 && progress != -.0)
        return Padding(
          padding: const EdgeInsets.all(24.0),
          child: Center(
              child: CircularProgressIndicator(
            value: progress,
          )),
        );
      return child;
    }),
  );
}

class ImageRoute extends StatelessWidget {
  /// Esta ruta muestra en detalle la imagen del gato. No era necesaria, pero
  /// como lo primero que hizo mi novia cuando le di la app fue clickar en una
  /// imagen la he puesto para mantener el buen UX
  final Cat item;

  const ImageRoute({Key? key, required this.item}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      /// Fondo transparente, que evoca que se puede volver atrás pulsando fuera
      /// de la imagen, inspirada en las imágenes de twitter
      backgroundColor: Color.fromRGBO(0, 0, 0, .75),
      body: Stack(children: [
        /// Seguramente esta forma no sea óptima, pero meh. Funciona
        GestureDetector(
          behavior: HitTestBehavior.opaque,
          onTap: () => Navigator.pop(context),
        ),
        Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Row(
              children: [Expanded(child: imageBuilder(item))],
            )
          ],
        ),
      ]),
    );
  }
}
