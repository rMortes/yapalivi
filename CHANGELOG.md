## 0.0.2

* Exposed basic ListView properties
* Added methods to interface with the provider's content and setting state from wherever

## 0.0.1

* Initial release 🎉
* Basic functionality included
