import 'package:flutter/cupertino.dart';
import 'package:yapalivi/widget/Yapalivi.dart';

abstract class IPagedProvider<ItemType> {
  final List<ItemType> content = <ItemType>[];
  int currentPage = 0;
  bool lastPageFetched = false;
  bool loadingPage = false;
  State<Yapalivi<ItemType>>? _currentYapaliviState;

  set currentYapaliviState(State<Yapalivi<ItemType>> newState) {
    _currentYapaliviState = newState;
  }

  ItemType? popFromContent(ItemType item) {
    ItemType? ret;
    _currentYapaliviState
        ?.setState(() => ret = content.remove(item) ? item : null);
    return ret;
  }

  void pushFromContent(ItemType item) =>
      _currentYapaliviState?.setState(() => content.add(item));

  void insertAtFromContent(int index, ItemType item) =>
      _currentYapaliviState?.setState(() => content.insert(index, item));

  void prependFromContent(ItemType item) => insertAtFromContent(0, item);

  void pushAllFromContent(List<ItemType> items) =>
      _currentYapaliviState?.setState(() => content.addAll(items));

  void insertAllAtFromContent(int index, List<ItemType> item) =>
      _currentYapaliviState?.setState(() => content.insertAll(index, item));

  void prependAllFromContent(List<ItemType> item) =>
      insertAllAtFromContent(0, item);

  void scrollEndHandler(bool endOfList) {
    if ((endOfList || content.isEmpty) && canLoadnewPages) loadNewPage();
  }

  Future<void> loadNewPage() {
    if (_currentYapaliviState == null)
      throw Exception("Current state not defined");
    loadingPage = true;
    return tryLoadingNewPage(_currentYapaliviState!.setState).whenComplete(() {
      loadingPage = false;
    });
  }

  bool get canLoadnewPages => !lastPageFetched && !loadingPage;

  @protected
  Future<void> tryLoadingNewPage(void Function(VoidCallback fn) setState);

  Future<List<ItemType>> fetchPage(int page);
}
