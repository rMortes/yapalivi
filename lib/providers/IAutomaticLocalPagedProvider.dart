import 'package:yapalivi/providers/IPagedProvider.dart';

abstract class IAutomaticLocalPagedProvider<ItemType>
    extends IPagedProvider<ItemType> {
  @override
  Future<List<ItemType>> fetchPage(int page) async {
    return (await shouldFetchLocal())
        ? fetchLocalPage(page)
        : fetchRemotePage(page);
  }

  Future<List<ItemType>> fetchLocalPage(int page);

  Future<List<ItemType>> fetchRemotePage(int page);

  Future<bool> shouldFetchLocal();
}
