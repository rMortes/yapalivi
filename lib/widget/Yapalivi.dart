import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:yapalivi/providers/IPagedProvider.dart';

class Yapalivi<ItemType> extends StatefulWidget {
  final IPagedProvider<ItemType> provider;
  final Widget Function(BuildContext context, int index, ItemType item)
      itemBuilder;
  final Widget? Function(
      BuildContext context,
      int previousIndex,
      int followingIndex,
      ItemType previousItem,
      ItemType followingItem)? separatorBuilder;
  final Widget? placeholderWidget;
  final Widget? noMoreItemsWidget;
  final Iterable<ItemType> Function(List<ItemType> elements)? filterFunction;

  // ListView properties

  final bool reverse;
  final bool addAutomaticKeepAlives;
  final bool addRepaintBoundaries;
  final bool addSemanticIndexes;
  final double? cacheExtent;
  final Clip clipBehavior;
  final ScrollController? controller;
  final DragStartBehavior dragStartBehavior;
  final double? itemExtent;
  final ScrollViewKeyboardDismissBehavior keyboardDismissBehavior;
  final EdgeInsetsGeometry? padding;
  final ScrollPhysics? physics;
  final bool? primary;
  final String? restorationId;
  final Axis scrollDirection;
  final int? semanticChildCount;
  final bool shrinkWrap;

  const Yapalivi(
      {Key? key,
      required this.provider,
      required this.itemBuilder,
      this.separatorBuilder,
      this.placeholderWidget,
      this.noMoreItemsWidget,
      this.filterFunction,
      this.reverse = false,
      this.addAutomaticKeepAlives = true,
      this.addRepaintBoundaries = true,
      this.addSemanticIndexes = true,
      this.cacheExtent,
      this.clipBehavior = Clip.hardEdge,
      this.controller,
      this.dragStartBehavior = DragStartBehavior.start,
      this.itemExtent,
      this.keyboardDismissBehavior = ScrollViewKeyboardDismissBehavior.manual,
      this.padding,
      this.physics,
      this.primary,
      this.restorationId,
      this.scrollDirection = Axis.vertical,
      this.semanticChildCount,
      this.shrinkWrap = false})
      : super(key: key);

  @override
  _YapaliviState<ItemType> createState() => _YapaliviState<ItemType>();
}

class _YapaliviState<ItemType> extends State<Yapalivi<ItemType>> {
  Iterable<ItemType> get content {
    return widget.filterFunction?.call(widget.provider.content) ??
        widget.provider.content;
  }

  int get listLength {
    return content.length +
        (!widget.provider.lastPageFetched && widget.placeholderWidget != null
            ? 1
            : 0) +
        (widget.provider.lastPageFetched && widget.noMoreItemsWidget != null
            ? 1
            : 0);
  }

  Widget getitemBuilder(int index) {
    if (index >= content.length) {
      if (widget.provider.lastPageFetched && widget.noMoreItemsWidget != null)
        return widget.noMoreItemsWidget!;
      else if (widget.placeholderWidget != null)
        return widget.placeholderWidget!;
    }
    return widget.itemBuilder(context, index, content.elementAt(index));
  }

  @override
  void initState() {
    super.initState();
    widget.provider.currentYapaliviState = this;
    widget.provider.loadNewPage();
  }

  @override
  Widget build(BuildContext context) {
    return NotificationListener<ScrollEndNotification>(
      child: widget.separatorBuilder == null
          ? ListView.builder(
              // Generic ListView properties
              addAutomaticKeepAlives: widget.addAutomaticKeepAlives,
              addRepaintBoundaries: widget.addRepaintBoundaries,
              addSemanticIndexes: widget.addSemanticIndexes,
              cacheExtent: widget.cacheExtent,
              clipBehavior: widget.clipBehavior,
              controller: widget.controller,
              dragStartBehavior: widget.dragStartBehavior,
              itemExtent: widget.itemExtent,
              keyboardDismissBehavior: widget.keyboardDismissBehavior,
              padding: widget.padding,
              physics: widget.physics,
              primary: widget.primary,
              restorationId: widget.restorationId,
              scrollDirection: widget.scrollDirection,
              semanticChildCount: widget.semanticChildCount,
              shrinkWrap: widget.shrinkWrap,
              reverse: widget.reverse,

              // Tweaked properties
              itemBuilder: (context, index) => getitemBuilder(index),
              itemCount: listLength)
          : ListView.separated(
              // Generic ListView properties
              addAutomaticKeepAlives: widget.addAutomaticKeepAlives,
              addRepaintBoundaries: widget.addRepaintBoundaries,
              addSemanticIndexes: widget.addSemanticIndexes,
              cacheExtent: widget.cacheExtent,
              clipBehavior: widget.clipBehavior,
              controller: widget.controller,
              dragStartBehavior: widget.dragStartBehavior,
              keyboardDismissBehavior: widget.keyboardDismissBehavior,
              padding: widget.padding,
              physics: widget.physics,
              primary: widget.primary,
              restorationId: widget.restorationId,
              scrollDirection: widget.scrollDirection,
              shrinkWrap: widget.shrinkWrap,
              reverse: widget.reverse,

              // Tweaked properties
              itemBuilder: (context, index) => getitemBuilder(index),
              separatorBuilder: (context, index) =>
                  widget.separatorBuilder!.call(context, index, index + 1,
                      content.elementAt(index), content.elementAt(index)) ??
                  Container(),
              itemCount: listLength),
      onNotification: (notification) {
        if (notification.metrics.atEdge) if (notification.metrics.pixels == 0.0)
          widget.provider.scrollEndHandler(false);
        else
          widget.provider.scrollEndHandler(true);
        return true;
      },
    );
  }
}
