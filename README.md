# yapalivi

**Y**et **A**nother **Pa**ginated **Li**st **Vi**ew and a **BIG WORK IN PROGRESS**

## Seriously, this is a BIG WORK IN PROGRESS

# WIP AHEAD, you've been warned

## About this package

**Yapalivi** aims at easign the process of building an infinitely scrollable in Flutter, while being as performant as `ListView.builder`. However, I myself am *extremely* new to Flutter, so most of this package will *not* follow any best practices whatsoever

## How to start?

### Creating your own provider

A **Yapalivi** component needs a `Provider` to retrieve the data. Right now, there are two possible Providers to extend:

- `IPagedProvider`: The most basic provider, with only the required functionality for the list view to work
- `IAutomaticLocalPagedProvider`: This provider can be used to provide an offline fallback for data retrieved from the network (Accepting suggestions for a better name)

### Builing the component

For the most basic functionality, a **Yapalivi** widget needs a provider and a builder. For convenience sake, the builder function will receive a context, the index of the item is being built, and the item itself.

Optionally, you can implement the following functions:

- `separatorBuilder`: If this function is not `null`, `ListView.separated` will be used instead of `ListView.builder`. This builder receives the context, the current index, the following index, the current item and the following item
- `filterFunction`: This function is a big fat WIP. It can be used to filter items, and only build those that are returned, but it will break things

Optionally, you can implement the following widgets:

- `placeholderWidget`: This widget will be displayed at the end of the list while there are more pages to be loaded
- `noMoreItemsWidget`: This widget will be rendered at the end of the list when the provider runs out of data to load

## Something doesn't work?

I'm absolutely sure that's the case. You can (pretty please!) report anything that's broken or you this it'd be a nice bonus to this project's [Gitlab](https://gitlab.com/rMortes/yapalivi)